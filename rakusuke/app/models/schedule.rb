class Schedule < ActiveRecord::Base

  belongs_to :person
  belongs_to :child
  belongs_to :transfer_destination

  # 送迎区分
  PICKUP_DIVS = [['送り', '01'], ['迎え', '02']]

  # ステータス区分
  STATUS_DIVS_FOR_MASTER = [['希望', '01'], ['確定', '02'], ['キャンセル', '03'], ['仮登録', '04']]
  STATUS_DIVS_FOR_PERSON = [['希望', '01'], ['キャンセル', '03']]

  # 利用時間帯(休日)
  USE_TIME_HOLIDAY_DIVS = [['10:00〜12:00', '04'],
                           ['14:00〜16:00', '05'],
                           ['10:00〜16:00', '06']]

  # 利用時間帯(平日)
  USE_TIME_WEEKDAY_DIVS = [['14:00〜16:30', '01'],
                           ['17:30〜18:30', '02'],
                           ['14:00〜18:30', '03']]

  def status_name
    unless self.status_div.blank?
      STATUS_DIVS_FOR_MASTER.rassoc(self.status_div)[0]
    end
  end

  def pickup_name
    unless self.pickup_div.blank?
      PICKUP_DIVS.rassoc(self.pickup_div)[0]
    end
  end

  def use_time_name
    use_time_divs = Schedule::USE_TIME_HOLIDAY_DIVS
    use_time_divs.concat(Schedule::USE_TIME_WEEKDAY_DIVS)
    unless self.use_time_div.blank?
      use_time_divs.rassoc(self.use_time_div)[0]
    end
  end

  def estimated_time_of_arrival_
    unless self.estimated_time_of_arrival.blank?
      self.estimated_time_of_arrival
    else
      " - "
    end
  end

  def get_out_of_school_time_
    unless self.get_out_of_school_time.blank?
      self.get_out_of_school_time
    else
      " - "
    end
  end

  def desired_time_
    unless self.desired_time.blank?
      self.desired_time
    else
      " - "
    end
  end

  def child_list_for_admin

    child_list = Child.limit(50).order("updated_at DESC")

    child_list_array = []

    child_list.each do |child|
      child_list_array.push([child.full_name, child.id])
    end

    child_list_array
  end

  def child_list_for_person(person)
    child_list = person.children.limit(50).order("updated_at DESC")

    child_list_array = []

    child_list.each do |child|
      child_list_array.push([child.full_name, child.id])
    end

    child_list_array
  end


  def transfer_destination_list_for_child
    transfer_destinations = self.child.transfer_destinations.limit(50).order("updated_at DESC")

    transfer_destination_list = []

    transfer_destinations.each do |transfer_destination|
      transfer_destination_list.push([transfer_destination.name, transfer_destination.id])
    end

    transfer_destination_list

  end

end
