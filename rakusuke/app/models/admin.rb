class Admin < Person

  before_create :set_type

  def set_type
    self.type = self.class
  end

end
