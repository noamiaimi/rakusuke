class TransferDestination < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :child
  has_many :schedules, :dependent => :destroy

  TRANSFER_DEST_DIVS = [['学校', '01'], ['自宅', '02']]

  def transfer_dest_name
    if self.transfer_dest_div
      TRANSFER_DEST_DIVS.rassoc(self.transfer_dest_div)[0]
    end
  end


end
