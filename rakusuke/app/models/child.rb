class Child < ActiveRecord::Base

  has_many :transfer_destinations, :dependent => :destroy
  has_many :schedule, :dependent => :destroy
  belongs_to :person

  def full_name
    self.last_name + " " +  self.first_name
  end

end
