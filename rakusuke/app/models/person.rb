class Person < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :children, :dependent => :destroy
  has_many :schedules, :dependent => :destroy

  before_create :set_type

  def set_type
    self.type = self.class
  end


end
