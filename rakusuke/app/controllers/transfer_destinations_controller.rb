class TransferDestinationsController < ApplicationController
  before_action :set_transfer_destination, only: [:show, :edit, :update, :destroy]

  # GET /transfer_destinations
  # GET /transfer_destinations.json
  def index
    # @transfer_destinations = TransferDestination.all
    p params[:child_id]
    @transfer_destinations = Child.find(params[:child_id]).transfer_destinations

  end

  # GET /transfer_destinations/1
  # GET /transfer_destinations/1.json
  def show
  end

  # GET /transfer_destinations/new
  def new
    @transfer_destination = TransferDestination.new
    @transfer_destination.child_id = params[:child_id].to_i
  end

  # GET /transfer_destinations/1/edit
  def edit
  end

  # POST /transfer_destinations
  # POST /transfer_destinations.json
  def create
    @transfer_destination = TransferDestination.new(transfer_destination_params)

    respond_to do |format|
      if @transfer_destination.save
        # format.html { redirect_to @transfer_destination, notice: 'Transfer destination was successfully created.' }
        format.html { redirect_to child_url(:id => @transfer_destination.child_id),
                                  notice: '登録されました。' }
        format.json { render :show, status: :created, location: @transfer_destination }
      else
        format.html { render :new }
        format.json { render json: @transfer_destination.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transfer_destinations/1
  # PATCH/PUT /transfer_destinations/1.json
  def update
    respond_to do |format|
      if @transfer_destination.update(transfer_destination_params)
        format.html { redirect_to @transfer_destination, notice: '更新されました。' }
        format.json { render :show, status: :ok, location: @transfer_destination }
      else
        format.html { render :edit }
        format.json { render json: @transfer_destination.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transfer_destinations/1
  # DELETE /transfer_destinations/1.json
  def destroy
    @transfer_destination.destroy
    respond_to do |format|
      # format.html { redirect_to transfer_destinations_url, notice: 'Transfer destination was successfully destroyed.' }
      format.html { redirect_to child_url(:id => @transfer_destination.child_id),
                                notice: '削除されました。' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer_destination
      @transfer_destination = TransferDestination.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_destination_params
      params.require(:transfer_destination).permit(:child_id, :name, :address, :memo, :deleted_at, :transfer_dest_div)
    end
end
