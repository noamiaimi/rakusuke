class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Deviseの認証ヘルパーメソッド
  before_filter :authenticate_person!
  # before_filter :authenticate_admin!

  include Jpmobile::ViewSelector



end
