class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :edit, :update, :destroy, :edit_transfer_dests]

  # GET /schedules
  # GET /schedules.json
  def index
    if current_person.is_a?(Admin)
      @schedules = Schedule.order("updated_at DESC")
    elsif current_person.is_a?(Person)
      @schedules = current_person.schedules.order("updated_at DESC")
    end
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @schedule = Schedule.new
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /edit_transfer_dests
  # POST /edit_transfer_dests.json
  def edit_transfer_dests
    parsed_params = params_for_action_create(schedule_params)
    logger.debug("edit_transfer_dests")
    logger.debug("schedule_params=#{schedule_params}")
    logger.debug("parsed_params=#{parsed_params}")
    logger.debug("@schedule_before=#{@schedule.inspect}")
    # @schedule = Schedule.new(parsed_params)
    @schedule.update(parsed_params)
    logger.debug("@schedule_after=#{@schedule.inspect}")
    @schedule
  end

  # POST /schedules
  # POST /schedules.json
  def create
    parsed_params = params_for_action_create(schedule_params)

    @schedule = Schedule.new(parsed_params)
    logger.debug("schedule_params=#{schedule_params}")
    logger.debug("@schedule=#{@schedule}")
    respond_to do |format|
      if @schedule.save
        format.html { redirect_to @schedule, notice: '登録されました。' }
        format.json { render :show, status: :created, location: @schedule }
      else
        format.html { render :new }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    respond_to do |format|
      parsed_params = params_for_action_create(schedule_params)

      if params[:id] == '0'
        @schedule = Schedule.new(parsed_params)
        if @schedule.save
          format.html { redirect_to @schedule, notice: '登録されました。' }
          format.json { render :show, status: :created, location: @schedule }
        else
          format.html { render :new }
          format.json { render json: @schedule.errors, status: :unprocessable_entity }
        end

      else
        if @schedule.update(parsed_params)
          format.html { redirect_to @schedule, notice: '更新されました。' }
          format.json { render :show, status: :ok, location: @schedule }
        else
          format.html { render :edit }
          format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to schedules_url, notice: '削除されました。' }
      format.json { head :no_content }
    end
  end

  def kakutei
    ids = params[:ids].split(",")
    logger.debug("ids= #{ids.inspect}")
    ids.each do |id|
      @schedule = Schedule.find(id)
      @schedule.status_div = '02'
      @schedule.save
    end
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      if params[:id] == '0'
        @schedule = Schedule.new
      else
        @schedule = Schedule.find(params[:id])
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:id, :person_id, :date, :pickup_div, :transfer_destination_id, :estimated_time_of_arrival, :child_id, :use_time_div, :get_out_of_school_time, :desired_time, :status_div, :memo, :deleted_at)
    end

    def params_for_action_create(params={})
      # 親のidをセット
      child = Child.find(params[:child_id])
      person = child.person
      params[:person_id] = person.id
      parsed_params = params.dup
      logger.debug("parsed_params=#{parsed_params}")
      return parsed_params
    end

  def params_for_action_update(params={})
    parsed_params = params.dup
    logger.debug("parsed_params=#{parsed_params}")
    return parsed_params
  end
end
