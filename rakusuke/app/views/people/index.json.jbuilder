json.array!(@people) do |person|
  json.extract! person, :id, :first_name, :last_name, :address, :email, :email_flg, :type, :memo, :deleted_at
  json.url person_url(person, format: :json)
end
