json.array!(@schedules) do |schedule|
  json.extract! schedule, :id, :person_id, :date, :pickup_div, :transfer_destination_id, :estimated_time_of_arrival, :child_id, :use_time_div, :get_out_of_school_time, :desired_time, :status, :memo, :deleted_at
  json.url schedule_url(schedule, format: :json)
end
