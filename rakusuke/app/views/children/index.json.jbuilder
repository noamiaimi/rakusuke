json.array!(@children) do |child|
  json.extract! child, :id, :person_id, :first_name, :last_name, :memo, :deleted_at
  json.url child_url(child, format: :json)
end
