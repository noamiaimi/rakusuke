json.array!(@transfer_destinations) do |transfer_destination|
  json.extract! transfer_destination, :id, :child_id, :name, :address, :memo, :deleted_at
  json.url transfer_destination_url(transfer_destination, format: :json)
end
