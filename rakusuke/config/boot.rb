# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
ENV["SECRET_KEY_BASE"] = "1b581f70f732c06e59e23de31f439a6d872eaaa29f5df2f816bd790c0b4535aa29ef979da1e72cf283055363262a498dc3b6983be5311a16c0e7ac3932cf9b55"

require 'bundler/setup' if File.exist?(ENV['BUNDLE_GEMFILE'])
