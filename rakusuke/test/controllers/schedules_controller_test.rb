require 'test_helper'

class SchedulesControllerTest < ActionController::TestCase
  setup do
    @schedule = schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schedule" do
    assert_difference('Schedule.count') do
      post :create, schedule: { child_id: @schedule.child_id, date: @schedule.date, deleted_at: @schedule.deleted_at, desired_time: @schedule.desired_time, estimated_time_of_arrival: @schedule.estimated_time_of_arrival, get_out_of_school_time: @schedule.get_out_of_school_time, memo: @schedule.memo, person_id: @schedule.person_id, pickup_div: @schedule.pickup_div, status: @schedule.status, transfer_destination_id: @schedule.transfer_destination_id, use_time_div: @schedule.use_time_div }
    end

    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should show schedule" do
    get :show, id: @schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schedule
    assert_response :success
  end

  test "should update schedule" do
    patch :update, id: @schedule, schedule: { child_id: @schedule.child_id, date: @schedule.date, deleted_at: @schedule.deleted_at, desired_time: @schedule.desired_time, estimated_time_of_arrival: @schedule.estimated_time_of_arrival, get_out_of_school_time: @schedule.get_out_of_school_time, memo: @schedule.memo, person_id: @schedule.person_id, pickup_div: @schedule.pickup_div, status: @schedule.status, transfer_destination_id: @schedule.transfer_destination_id, use_time_div: @schedule.use_time_div }
    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should destroy schedule" do
    assert_difference('Schedule.count', -1) do
      delete :destroy, id: @schedule
    end

    assert_redirected_to schedules_path
  end
end
