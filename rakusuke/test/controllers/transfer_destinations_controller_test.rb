require 'test_helper'

class TransferDestinationsControllerTest < ActionController::TestCase
  setup do
    @transfer_destination = transfer_destinations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transfer_destinations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transfer_destination" do
    assert_difference('TransferDestination.count') do
      post :create, transfer_destination: { address: @transfer_destination.address, child_id: @transfer_destination.child_id, deleted_at: @transfer_destination.deleted_at, memo: @transfer_destination.memo, name: @transfer_destination.name }
    end

    assert_redirected_to transfer_destination_path(assigns(:transfer_destination))
  end

  test "should show transfer_destination" do
    get :show, id: @transfer_destination
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transfer_destination
    assert_response :success
  end

  test "should update transfer_destination" do
    patch :update, id: @transfer_destination, transfer_destination: { address: @transfer_destination.address, child_id: @transfer_destination.child_id, deleted_at: @transfer_destination.deleted_at, memo: @transfer_destination.memo, name: @transfer_destination.name }
    assert_redirected_to transfer_destination_path(assigns(:transfer_destination))
  end

  test "should destroy transfer_destination" do
    assert_difference('TransferDestination.count', -1) do
      delete :destroy, id: @transfer_destination
    end

    assert_redirected_to transfer_destinations_path
  end
end
