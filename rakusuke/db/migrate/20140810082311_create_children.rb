class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.integer :person_id
      t.string :first_name
      t.string :last_name
      t.text :memo
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
