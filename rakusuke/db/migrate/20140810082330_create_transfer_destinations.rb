class CreateTransferDestinations < ActiveRecord::Migration
  def change
    create_table :transfer_destinations do |t|
      t.integer :child_id
      t.string :name
      t.string :address
      t.text :memo
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
