class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :person_id
      t.date :date
      t.string :pickup_div
      t.integer :transfer_destination_id
      t.string :estimated_time_of_arrival
      t.integer :child_id
      t.string :use_time_div
      t.string :get_out_of_school_time
      t.string :desired_time
      t.string :status_div
      t.text :memo
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
