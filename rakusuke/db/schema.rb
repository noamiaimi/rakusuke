# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140811045917) do

  create_table "children", force: true do |t|
    t.integer  "person_id"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "memo"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people", force: true do |t|
    t.string   "first_name",             default: "",    null: false
    t.string   "last_name",              default: "",    null: false
    t.string   "address",                default: "",    null: false
    t.string   "email",                  default: "",    null: false
    t.boolean  "email_flg",              default: false
    t.string   "type"
    t.text     "memo"
    t.datetime "deleted_at"
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "people", ["email"], name: "index_people_on_email", unique: true, using: :btree
  add_index "people", ["reset_password_token"], name: "index_people_on_reset_password_token", unique: true, using: :btree

  create_table "schedules", force: true do |t|
    t.integer  "person_id"
    t.date     "date"
    t.string   "pickup_div"
    t.integer  "transfer_destination_id"
    t.time     "estimated_time_of_arrival"
    t.integer  "child_id"
    t.string   "use_time_div"
    t.time     "get_out_of_school_time"
    t.time     "desired_time"
    t.string   "status_div"
    t.text     "memo"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transfer_destinations", force: true do |t|
    t.integer  "child_id"
    t.string   "name"
    t.string   "address"
    t.text     "memo"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "transfer_dest_div"
  end

end
